#!/usr/bin/env python3

import rospy
from sensor_msgs.msg import Image
from carla_msgs.msg import CarlaEgoVehicleStatus
from cv_bridge import CvBridge, CvBridgeError
import cv2
import time
import h5py
import numpy as np

bridge = CvBridge()
f = h5py.File("{}.hdf5".format(int(time.time())), "w")
img_dataset = f.create_dataset("images", (20, 160, 400), maxshape=(None,160, 400), dtype='uint8')
v_status_dataset = f.create_dataset("v_status", (20, 3), maxshape=(None,3), dtype=np.float32)
tmp_images = []
tmp_v_status = []

def callback(msg):
    v_status = rospy.wait_for_message('/carla/ego_vehicle/vehicle_status', CarlaEgoVehicleStatus, 1)
    r = rospy.Rate(2)
    try:
        img = bridge.imgmsg_to_cv2(msg, "bgr8")
    except:
        print(e)
    else:
        time = msg.header.stamp
        s_img = cv2.resize(img, (400, 300))
        c_img = s_img[140:300, 0:400]
        b_img = cv2.blur(c_img, (4,4))
        gy_img = cv2.cvtColor(b_img, cv2.COLOR_BGR2GRAY)
        store_h5(gy_img, [v_status.control.throttle, v_status.control.steer, v_status.control.brake])
        r.sleep()


def store_h5(image, v_status):
    tmp_images.append(image)
    tmp_v_status.append(v_status)
    print("Vehicle status: {} - {} - {}".format(v_status[0], v_status[1], v_status[2]))
    if len(tmp_images) == 20:
        print("storing")
        img_dataset[-20:] = np.array(tmp_images)
        img_dataset.resize(img_dataset.shape[0]+20, axis=0)
        tmp_images.clear()
        v_status_dataset[-20:] = np.array(tmp_v_status)
        v_status_dataset.resize(v_status_dataset.shape[0]+20, axis=0)
        tmp_v_status.clear()
        print("dataset shape is {}".format(img_dataset.shape))
    else:
        print("accumulating...")

rospy.init_node('image_saver')
rospy.Subscriber('/carla/ego_vehicle/rgb_front/image', Image, callback)
rospy.spin()